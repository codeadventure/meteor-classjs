Class.js for Meteor
===================

***Lightweight JavaScript class system for Meteor.js***

100% no wrappers, same performance as hand-written pure JS classes.
Exposes a beautiful API and gives classes and methods speaking names for debugging!

## Examples

Define a class
--------------
```JavaScript

Class('lib.Person', {

    STATIC: {
      AGE_OF_MAJORITY: 18
    },

    initialize: function(name, age) {
      this.name = name;
      this.age = age;
    },

    sayHello: function() {
      console.log('Hello from ' + this.name + '!');
    },

    drinkAlcohol: function() {
      this.age < lib.Person.AGE_OF_MAJORITY ?
        console.log('Too young! Drink milk instead!') :
        console.log('Whiskey or beer?');
    }
});

var john = new lib.Person('John', 16);
john.sayHello(); //log "Hello from John!"
john.drinkAlcohol(); //log "Too young! Drink milk instead!"
```

Extend and Implement other Classes
----------------------------------
```JavaScript

Class('lib.Dreamy', {

    dream: 'default',

    describeDream: function() {
      return "..it is about: " + this.dream;
    }
});

Class('lib.Awakable', {

    wakeUp: function() {
      console.log('Wake up!');
    }

});

var Dreamer = Class('lib.Dreamer', {

    Extends: lib.Person, // person is super class (prototypal inheritance)
    Implements: [lib.Dreamy, lib.Awakable], // mixin prototypes of other classes

    initialize: function(name, age, dream) {
      Dreamer.Super.call(this, name, age);
      this.dream = dream;
    },

    sayHello: function() {
      Dreamer.Super.prototype.sayHello.call(this);
      console.log('I dream of ' + this.describeDream() + '!');
    }
});


var sylvester = new lib.Dreamer('Sylvester', 30, 'eating Tweety');
sylvester.sayHello(); //log "Hello from Sylvester! I dream of eating Tweety!"
sylvester.wakeUp(); //log "Wake up!"
```

Inheritance of static properties
--------------------------------
Static properties of the super class are automatically copied to the
subclass and then merged with the static properties defined by the subclass.
This is what most programmers would expect from traditional inheritance.

```JavaScript

var Spaceship = Class('lib.Spaceship', {

    STATIC: {
      MIN_SPEED: 0,
      MAX_SPEED: 100,
    },

    initialize: function() {
      console.log('Spaceship speed min: ' + Spaceship.MIN_SPEED + ' max: ' + Spaceship.MAX_SPEED);
    }
  });

var Enterprise = Class('lib.Enterprise', {

    Extends: lib.Spaceship,

    STATIC: {
      // overrides the static property of the super class
      MAX_SPEED: 99999,
    },

    initialize: function() {
      console.log('Enterprise speed min: ' + Enterprise.MIN_SPEED + ' max: ' + Enterprise.MAX_SPEED);
    }
});

var spaceship = new lib.Spaceship(); // logs: Spaceship speed min: 0 max: 100
var enterprise = new lib.Enterprise(); // logs: Enterprise speed min: 0 max: 99999
```

Interfaces
--------------------------------
Optionally you can start defining interfaces for your classes. These add simple
runtime checks if you defined all required methods on your class. Does not slow
down your code because if you define the methods they will simply override the
ones of the interface.

```JavaScript
Interface('ICommand', {

  execute: Function,
  undo: Function

});

var Command = Class({

  Implements: ICommand,

  execute: function() {
    console.log('executing command');
  }

});

var command = new Command();
command.execute(); // logs: executing command

try {
  command.undo();
} catch(error) {
  console.debug(error.message); // logs: Missing implementation for <Command::undo> required by interface ICommand
}
```

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding
style. Add unit tests for any new or changed functionality. Lint and test
your code using [grunt](https://github.com/gruntjs/grunt).

## Release History
Version 0.1.0

## License
Copyright (c) 2013 Code Adventure
Licensed under the MIT license.
