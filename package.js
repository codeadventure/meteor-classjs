Package.describe({
  summary: 'Lightweight JavaScript class system for Meteor.js'
});

Package.on_use(function(api) {

  // add files for client and server
  api.add_files(
    [
      'source/Class.js',
      'source/Interface.js'
    ],
    ['client', 'server']
  );

});