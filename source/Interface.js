(function (globalNamespace) {
  "use strict";

    /**
     * @constructor
     */
    var ImplementationMissingError = function (message) {
      this.name = "ImplementationMissingError";
      this.message = (message || "");
    };
    
    ImplementationMissingError.prototype = Error.prototype;

    function createExceptionThrower(interfaceName, methodName) {
      return function() {
        var message = 'Missing implementation for <' + this + '::' + methodName + '> defined by interface ' + interfaceName;

        throw new ImplementationMissingError(message);
      };
    }

    var Interface = function(path, definition, local) {

      if(!definition) {
        throw new Error('Please provide a definition for your interface ' + path)
      }

      if(typeof path !== 'string') {
        throw new Error('Please give your interface a name. Pass "true" as last parameter to avoid global namespace pollution');
      }

      var interfaceName = path.substr(path.lastIndexOf('.') + 1),
          methodName,
          property;

      /*jslint evil: true */
      var InterfaceConstructor = new Function('return function ' + interfaceName + '() {}')();

      for(methodName in definition) {

        if(definition.hasOwnProperty(methodName)) {
          InterfaceConstructor.prototype[methodName] = createExceptionThrower(path, methodName);
        }
      }

      if(!local) {
        Class.namespace(path, InterfaceConstructor);
      }

      InterfaceConstructor.toString = function () { return interfaceName; };

      return InterfaceConstructor;
    };

  Interface.ImplementationMissingError = ImplementationMissingError;

  globalNamespace.Interface = Interface;


}(this));